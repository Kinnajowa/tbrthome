/**
 * Created by Marc on 20.07.2017.
 */
export class User {
  id: number;
  name: string;
  secNum: number;
  oncreated: string;
}
