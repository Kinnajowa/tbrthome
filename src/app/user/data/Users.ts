import {User} from "../user";

 export const USER: User[] = [
    {
      id: 0,
      name: 'Default',
      secNum: 72864,
      oncreated: '17:37'
    },
    {
      id: 1,
      name: 'Jannik',
      secNum: 54356,
      oncreated: '09:06'
    },
    {
      id: 2,
      name: 'Laura',
      secNum: 87209,
      oncreated: '16:44'
    },
    {
      id: 3,
      name: 'Isa',
      secNum: 29844,
      oncreated: '16:45'
    }]

