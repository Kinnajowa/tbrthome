/**
 * Created by Marc on 20.07.2017.
 */
import {Injectable} from '@angular/core';
import {User} from "./user";
import {USER} from "./data/Users";

@Injectable()
export class UserService{
  curruser = Number(localStorage.getItem('uid'));
  getUser(id: number): User {
    return USER.find(user => user.id === id)

  }
  getCurrUser(): User {
    return this.getUser(this.curruser)
  }
  setCurrUser(id: number): User {
    localStorage.setItem('uid', String(id))
    return this.getCurrUser();
  }
}
