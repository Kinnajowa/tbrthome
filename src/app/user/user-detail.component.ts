/**
 * Created by Jannik on 19.07.2017.
 */
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";
import {Location} from "@angular/common";

import 'rxjs/add/operator/switchMap';
import {UserService} from "./user.service";
import {User} from "./user";

@Component({
  templateUrl: '/templates/user-detail.component.html'
})
export class UserDetailComponent {
  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private location: Location
  ){}
  /**ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.userService.getUser(+params.get('id')))
      .subscribe(user => this.viewuser = user)
  }**/
}
