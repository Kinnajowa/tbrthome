/**
 * Created by Marc on 19.07.2017.
 */
import {Component} from '@angular/core';
import {User} from "../user/user";
import {UserService} from "../user/user.service";
import {rootRoute} from "@angular/router/src/router_module";
import {NavbarComponent} from "../root/navbar.component";

@Component({
  selector: 'home',
  templateUrl: './templates/home.component.html'
})
export class HomeComponent{
  constructor(private userService: UserService){}
  setUser(id: number): void {
    this.userService.setCurrUser(id);
    NavbarComponent.curruser = this.userService.getCurrUser()
  }
}
