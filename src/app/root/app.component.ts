import {Component, OnInit} from '@angular/core';
import {UserService} from "../user/user.service";

@Component({
  selector: 'app-root',
  template: '<navbar></navbar><router-outlet></router-outlet>',
  styleUrls: [
    './styles/app.component.css',
  ]
})
export class AppComponent implements OnInit{
  constructor(private userService: UserService){}
  ngOnInit(){
   this.userService.setCurrUser(0)
  }
  title = 'tbrt.de';
}
