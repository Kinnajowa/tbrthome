import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';

import { AppComponent } from './app.component';
import {NavbarComponent} from "./navbar.component";
import {HomeComponent} from "../home/home.component";
import {UserDetailComponent} from "../user/user-detail.component";
import {UserService} from "../user/user.service";
import {UsersComponet} from "../user/users.component";

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    UserDetailComponent,
    UsersComponet
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: '', component: HomeComponent},
      {path: 'user/:id', component: UserDetailComponent},
      {path: 'users', component: UsersComponet}
    ])
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
