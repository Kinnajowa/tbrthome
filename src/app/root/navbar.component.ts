/**
 * Created by Marc on 18.07.2017.
 */
import {Component, OnChanges, OnInit} from '@angular/core';
import {User} from "../user/user";
import {AppComponent} from "./app.component";
import {UserService} from "../user/user.service";


@Component({
  selector: 'navbar',
  templateUrl: './templates/navbar.component.html'
})
export class NavbarComponent implements OnInit{
  constructor(private userService: UserService){}
  public curruser: User;
  currname: string;
  currid: number;
  setUser(): void {
    this.curruser = this.userService.getCurrUser()
    this.currname = this.curruser.name;
    this.currid = this.curruser.id
  }
  ngOnInit(){
    this.setUser();
  }
}
